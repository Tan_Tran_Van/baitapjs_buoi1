// BÀI TẬP 1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
//
//Đầu vào: Lương 1 ngày 100.000; Số ngày làm việc: 24 ngày
//Các bước tiến hành:
//+ B1: Nhập Lương 1 ngày: luongMotNgay = 100.000
//+ B2: Nhập số ngày làm việc: soNgayLamViec = 24;
//+ B3: Tính tiền lương nhân viên: luongNhanVien = luongMotNgay * soNgayLamViec = 2.400.000;
//Đầu ra: Xuất tiền lương nhân viên

var luongMotNgay = 100000;
var soNgayLamViec = 24;
var luongNhanVien = luongMotNgay * soNgayLamViec;
console.log("Bài tập 1: Lương nhân viên = ", luongNhanVien);

// BÀI TẬP 2: TÍNH GIÁ TRỊ TRUNG BÌNH
//
//Đầu vào: Nhập 5 số thực:
//+ numberOne = 20 ; numberTwo = 25; numberThree = 30; numberFour = 15 ; numberFive = 10
//Các bước tiến hành:
//+ B1: Nhập 5 số thực;
//+ B2: Tính tổng 5 số thực trên; sumFiveNuber = 100
//+ B3: Tính giá trị trung bình của 5 số thực trên = Tổng 5 số thực / 5; averageNumber = sumFiveNuber/5 = 20
//Đầu ra: Xuất giá trị trung bình của 5 số thực trên;

var numberOne = 20;
var numberTwo = 25;
var numberThree = 30;
var numberFour = 15;
var numberFive = 10;
var sumFiveNumber =
  numberOne + numberTwo + numberThree + numberFour + numberFive;
var averageNumber = sumFiveNumber / 5;
console.log("Bài tập 2: Giá Trị Trung Bình = ", averageNumber);

// BÀI TẬP 3: QUY ĐỔI TIỀN USD SANG VNĐ
//
//Đầu vào: Giá trị đồng USD với VND: unitUsd = 23.500; Số tiền USD cần tính: numberUsd = 30 usd;
//Các bước tiến hành:
//+ B1: Nhập giá trị unitUsd và Số tiền USD cần quy đổi VND: numberUsd = 30;
//+ B2: Tính tiền quy đổi sang VND: numberVnd = unitUsd*numberUsd =
//Đầu ra: Xuất Số tiền quy đổi trên;

var unitUsd = 23500;
var numberUsd = 30;
var numberVnd = numberUsd * unitUsd;
console.log("Bài tập 3: Quy đổi tiền 30USD sang VNĐ = ", numberVnd);

// BÀI TẬP 4: TÍNH DIỆN TÍCH VÀ CHU VI HÌNH CHỮ NHẬT
//
//Đầu vào:  chieuDai = 50; chieuRong = 30;
//Các bước tiến hành:
//+ B1: Nhập chieuDai = 50; chieuRong = 30;
//+ B2: Tính Diện tích và Chu vi hình chữ nhật:
//    dienTich = chieuDai*chieuRong = 1500
//    chuVi = (chieuDai+chieuRong)*2 = 3000
//Đầu ra: Xuất giá trị Dien tich và Chu vi hình chữ nhật;

var chieuDai = 50;
var chieuRong = 30;
var dienTich = chieuDai * chieuRong;
console.log("Bài tập 4: Diện tích hình chữ nhật = ", dienTich);
var chuVi = (chieuDai + chieuRong) * 2;
console.log("Chu vi hình chữ nhật = ", chuVi);

// BÀI TẬP 5: TÍNH TỔNG CHỮ SỐ CÓ 2 KÝ SỐ
//
//Đầu vào: Chữ số có 2 ký số: n = 59
//Các bước tiến hành:
//+ B1: Nhập vào chữ số có 2 ký số: n = 59;
//+ B2: Tách hàng đơn vị và hàng chục của chữ số n:
//    hangDonVi = n%10 = 9;
//    hangChuc = Math.floor(n/10);
//+ B3: Tính tổng 2 ký số tongKySo = hangDonVi + hangChuc = 14
//Đầu ra: Xuất giá trị tổng 2 ký số;

var n = 59;
var hangDonVi = n % 10;
var hangChuc = Math.floor(n / 10);
var tongKySo = hangChuc + hangDonVi;
console.log(
  "Bài tập 5: Tổng chữ số có 2 ký số: n = 59 là: tongKySo = ",
  tongKySo
);
